import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import LeftNav from "../LeftNav.vue";

import { authGuard } from "@/auth/authGaurd";
import Overview from "../components/Overview.vue";
import Program from "../components/Program.vue";
import Clients from "../components/Clients.vue";
import Trainers from "../components/Trainers.vue";
import Groups from "../components/Groups.vue";
import Profile from "../views/Profile.vue";
import Login from "@/views/Login.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [

  {
    path: "/callback",
    name: "callback",
    component: Overview
  },
  {
    path: "",
    name: "overview",
    component: Overview
  },
  {
    path: "/program",
    name: "program",
    beforeEnter: authGuard,
    component: Program
  },
  {
    path: "/clients",
    name: "clients",
    beforeEnter: authGuard,
    component: Clients
  },
  {
    path: "/trainers",
    name: "trainers",
    beforeEnter: authGuard,
    component: Trainers
  },
  {
    path: "/groups",
    name: "groups",
    beforeEnter: authGuard,
    component: Groups
  },
  {
    name: "login",
    path: "/login",
    beforeEnter: authGuard,
    component: Overview
  },
  {
    name: "profile",
    path: "/profile",

    component: Profile
  }

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
