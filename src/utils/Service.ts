function serviceFunc<T>(type: { new(): T}, name: string): {install: (Vue: any, options: object) => void} {
  return {install: (Vue: any, options: object) => {
      Vue.prototype[name] = new type();
    }}
}
export default serviceFunc;