
import ApiService from "./ApiService";

export default class ExercisesApiService extends ApiService{

    constructor() {
        super();
        this.extendBaseUrl("/exercise");
    }

    async get() {
        return this.getRequest(``);
    }
    async getMuscleGroups() {
        return this.getRequest(`muscle_groups`);
    }

    async delete(productId: number) {
        return this.deleteRequest(`${productId}`);
    }

}