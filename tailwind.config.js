

module.exports = {
  theme: {
    container: {
      center: true
    },    variants: {
      extend: {
        // ...
        scale: ['active', 'group-hover'],
      }
    },
    extend: {
      fontFamily: {
        sans: ['cairo', 'sans-serif'],
      },
      colors: {
        'blue': '#1DA1F2',
        'darkblue': '#2795D9',
        'lightblue': '#EFF9FF',
        'dark': '#657786',
        'light': '#AAB8C2',
        'lighter': '#E1E8ED',
        'lightest': '#F5F8FA',
        'black':'#312F2F',
        'yellow':'#ffd600'
      }
    },
    plugins: [
      require('@tailwindcss/ui'),
    ]
  }
};
