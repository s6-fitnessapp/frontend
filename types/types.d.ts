import {VueConstructor} from 'vue'
import { VueAuth } from '../src/auth/VueAuth'
import ApiServices from '@/utils/ApiService/ApiServices';


declare module 'vue/types/vue' {
    interface Vue {
        $auth: VueAuth
        $apiService: ApiServices;
    }
}
